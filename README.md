# Phone Number Normaliser

This Gophercise exercise (#8) required implementing a basic phonenumber normaliser using three different SQL libraries:

1. the standard library `database/sql` package
1. `sqlx`, a third-party library
1. `gorm`, a Go ORM (Object Relationship mapper)

# Tools

The tools I found useful where [Postgres.app](https://postgresapp.com/documentation/), [Postico](https://eggerapps.at/postico/), and to work with SQLite [DB Browser](https://sqlitebrowser.org/).