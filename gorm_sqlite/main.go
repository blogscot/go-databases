package main

import (
	"strings"
	"unicode"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// User contains a user's info.
type User struct {
	gorm.Model
	PhoneNumber string
}

var phoneNumbers = []string{
	"1234567890",
	"123 456 7891",
	"(123) 456 7892",
	"(123) 456-7893",
	"123-456-7894",
	"123-456-7890",
	"1234567892",
	"(123)456-7892",
}

func main() {
	db, err := gorm.Open("sqlite3", "users.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&User{})

	// Create
	for _, number := range phoneNumbers {
		db.Create(&User{PhoneNumber: number})
	}

	var users []User
	db.Find(&users)

	uniqueNumbers := make(map[string]struct{})

	// Normalise
	for _, user := range users {
		n := normalise(user.PhoneNumber)

		if _, ok := uniqueNumbers[n]; ok {
			db.Delete(&user)
		} else {
			uniqueNumbers[n] = struct{}{}
			if user.PhoneNumber != n {
				db.Model(&user).Update("phone_number", n)
			}
		}
	}
}

func normalise(phoneNumber string) string {
	return strings.Map(func(r rune) rune {
		if !unicode.IsDigit(r) {
			return -1
		}
		return r
	}, phoneNumber)
}
