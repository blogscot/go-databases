package main

const (
	schema = `
	CREATE TABLE users (
		id SERIAL PRIMARY KEY NOT NULL UNIQUE,
		phone_number character varying(255) NOT NULL
	);
	`
	insertUserQuery = `
INSERT INTO 
	users (phone_number) 
	VALUES ($1);
`
	usersQuery = `
-- Get all user info
SELECT 
	* 
FROM users;
`
	updatePhoneNumberQuery = `
UPDATE users 
  SET phone_number = $1 
WHERE phone_number = $2;
`
	deleteUserQuery = `
DELETE from users 
WHERE id=$1;
`
)
