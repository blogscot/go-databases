package main

import (
	"database/sql"
	"log"
	"strings"
	"unicode"

	_ "github.com/lib/pq"
)

type user struct {
	ID          int
	phoneNumber string
}

var (
	phoneNumbers = []string{
		"1234567890",
		"123 456 7891",
		"(123) 456 7892",
		"(123) 456-7893",
		"123-456-7894",
		"123-456-7890",
		"1234567892",
		"(123)456-7892",
	}
)

func main() {

	db := connectDB()
	defer db.Close()

	createTable(db)
	loadData(db)

	stmt, err := db.Prepare(usersQuery)
	check("prepare error: ", err)
	defer stmt.Close()

	var users []user
	rows, err := stmt.Query()
	defer rows.Close()

	var id int
	var phoneNumber string
	for rows.Next() {
		if err := rows.Scan(&id, &phoneNumber); err != nil {
			log.Fatal(err)
		}
		users = append(users, user{ID: id, phoneNumber: phoneNumber})
	}

	uniqueNumbers := make(map[string]struct{})

	for _, user := range users {
		n := normalise(user.phoneNumber)

		if _, ok := uniqueNumbers[n]; ok {
			_, err := db.Exec(deleteUserQuery, user.ID)
			check("delete error:", err)
		} else {
			uniqueNumbers[n] = struct{}{}
			if user.phoneNumber != n {
				db.Exec(updatePhoneNumberQuery, n, user.phoneNumber)
				check("update error: ", err)
			}
		}
	}
}

func connectDB() (db *sql.DB) {
	db, err := sql.Open("postgres", "user=postgres dbname=gophercise sslmode=disable")
	check("Error: The data source arguments are not valid", err)

	err = db.Ping()
	check("Error: Could not establish a connection with the database", err)
	return
}

func createTable(db *sql.DB) {
	_, err := db.Exec("DROP TABLE IF EXISTS users;")
	_, err = db.Exec(schema)
	check("error creating table: ", err)
}

func loadData(db *sql.DB) {
	for _, number := range phoneNumbers {
		stmt, err := db.Prepare(insertUserQuery)
		check("Insert error: ", err)
		_, err = stmt.Exec(number)
		check("Exec error: ", err)
	}
}

func normalise(phoneNumber string) string {
	return strings.Map(func(r rune) rune {
		if !unicode.IsDigit(r) {
			return -1
		}
		return r
	}, phoneNumber)
}

func check(msg string, err error) {
	if err != nil {
		log.Fatal(msg, err)
	}
}
