package main

import (
	"log"
	"strings"
	"unicode"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type user struct {
	ID          int    `db:"id"`
	PhoneNumber string `db:"phone_number"`
}

var (
	phoneNumbers = []string{
		"1234567890",
		"123 456 7891",
		"(123) 456 7892",
		"(123) 456-7893",
		"123-456-7894",
		"123-456-7890",
		"1234567892",
		"(123)456-7892",
	}
)

func main() {

	db := connectDB()
	defer db.Close()

	createTable(db)
	loadData(db)

	var users []user
	err := db.Select(&users, usersQuery)
	check("select error: ", err)

	uniqueNumbers := make(map[string]struct{})

	for _, user := range users {
		n := normalise(user.PhoneNumber)

		if _, ok := uniqueNumbers[n]; ok {
			db.MustExec(deleteUserQuery, user.ID)
		} else {
			uniqueNumbers[n] = struct{}{}
			if user.PhoneNumber != n {
				db.MustExec(updatePhoneNumberQuery, n, user.PhoneNumber)
			}
		}
	}
}

func connectDB() (db *sqlx.DB) {
	db, err := sqlx.Connect("postgres", "user=postgres dbname=gophercise sslmode=disable")
	check("Error: The data source arguments are not valid", err)
	return
}

func createTable(db *sqlx.DB) {
	_ = db.MustExec("DROP TABLE IF EXISTS users;")
	_ = db.MustExec(schema)
}

func loadData(db *sqlx.DB) {
	for _, phoneNumber := range phoneNumbers {
		db.MustExec(insertUserQuery, phoneNumber)
	}
}

func normalise(phoneNumber string) string {
	return strings.Map(func(r rune) rune {
		if !unicode.IsDigit(r) {
			return -1
		}
		return r
	}, phoneNumber)
}

func check(msg string, err error) {
	if err != nil {
		log.Fatal(msg, err)
	}
}
